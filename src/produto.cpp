#include "produto.hpp"
#include <iostream>

using namespace std;

Produto::Produto(vector<string> &categoria, string nome, float preco, int qtd){
	//cout<<"Construtor classe produto"<<endl;
	set_categoria(categoria);
	set_nome(nome);
	set_preco(preco);
	set_qtd(qtd);
}
Produto::Produto(string categori, string nome, float preco, int qtd){
	//cout<<"Construtor 2 classe produto"<<endl;
	set_categori(categori);
	set_nome(nome);
	set_preco(preco);
	set_qtd(qtd);
}
Produto::~Produto(){
	//cout<<"Destrutor classe produto"<<endl;
}

string Produto::get_categoria(){
	for(int i=0; i<categoria.size(); i++){
		return categoria[i];
	}
}

void Produto::set_categoria(vector<string> &categoria){
	for(int i=0; i<categoria.size(); i++){
		this->categoria=categoria;
	}
}

string Produto::get_categori(){
	return categori;
}

void Produto::set_categori(string categori){
	this->categori=categori;
}
string Produto::get_nome(){
	return nome;
}

void Produto::set_nome(string nome){
	this->nome=nome;
}

float Produto::get_preco(){
	return preco;
}

void Produto::set_preco(float preco){
	this->preco=preco;
}
int Produto::get_qtd(){
	return qtd;
}
void Produto::set_qtd(int qtd){
	this->qtd=qtd; 
}
void Produto::imprime_dados(){
	cout << "Categoria: " << get_categori() << endl;
	cout << get_nome() << endl;
	cout << "R$:" << get_preco() << ",00" << endl;
	cout <<"Quantidade: " << get_qtd() << endl;
}
