#include "cliente.hpp"
#include <iostream>

using namespace std;


Cliente::Cliente(string nome, string telefone, string email, string cpf, int socio){
    //cout<<"Construtor classe cliente"<<endl;
	set_nome(nome);
	set_telefone(telefone);
	set_email(email);
	set_cpf(cpf);
	set_socio(socio);
}
Cliente::~Cliente(){
	//cout<<"Destrutor classe cliente"<<endl;
}

string Cliente::get_nome(){
	return nome;
}

void Cliente::set_nome(string nome){
	this->nome=nome;
}

string Cliente::get_telefone(){
	return telefone;
}

void Cliente::set_telefone(string telefone){
        this->telefone=telefone;
}

string Cliente::get_email(){
        return email;
}

void Cliente::set_email(string email){
        this->email=email;
}

string Cliente::get_cpf(){
        return cpf;
}
void Cliente::set_cpf(string cpf){
	    this->cpf=cpf;
}

int Cliente::get_socio(){
	return socio;
}

void Cliente::set_socio(int socio){
	this->socio=socio;
}



