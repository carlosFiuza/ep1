#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include "produto.hpp"
#include "cliente.hpp"

using namespace std;

template <typename E>

E getInput(){
    while(true){
    E valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
  }
}
string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}
bool maior_menor(int A, int B){
	return A > B;
}
void criaArquivoEstoque(vector<string> &categoria, vector<string> &tdscategorias, string nome, float preco, int qtd){

	ofstream saida;
	ifstream entrada;
	string cat;
	int w, qtda=0;
	int verificaSeTemCatCadas;
	int verificaSeTemProd;
	for(int i=0; i<categoria.size(); i++){
		verificaSeTemCatCadas = 0;
		for(int k=0; k<tdscategorias.size(); k++){
			if(categoria[i]==tdscategorias[k]){
				verificaSeTemProd = 0;
				verificaSeTemCatCadas = 1;
				entrada.open("doc/categorias/" + categoria[i]);
				if(entrada.is_open()){
					w=0;
					while(entrada&&w!=1){
						getline(entrada,cat);
						if(nome==cat){
							verificaSeTemProd = 1;
							w=1;
							entrada.close();
							entrada.open("doc/produtos/" + nome);
							if(entrada.is_open()){
								entrada >> cat;
								entrada >> qtda;
								entrada.close();
							}
							else {
								cout << "falha na abertura arquivo entrada para pegar quantidade" << endl;
							}
							saida.open("doc/produtos/" + nome);
							if(saida.is_open()){
								saida << nome << endl;
								qtda = qtd + qtda;
								saida << qtda << endl << preco << endl;
								saida.close();
							}
							else { cout << "Falha na saida do arquivo produtos + nome" << endl;
							}
						}
					}
				}
				else { cout << "Falha na entrada e saida do arquivo" << endl;
				throw 1;
				}
			}
		}
		if(verificaSeTemCatCadas==0){
			saida.open("doc/categorias/" + categoria[i]);
			if(saida.is_open()){
				saida << nome << endl;
				saida.close();
			}
			else { 
				cout << "Falha abertura arquivo saída para inscrição de nova categoria" << endl;
				throw 1;
			}
		    saida.open("doc/categorias/Tdscategorias.txt", ios::app);
			if(saida.is_open()){
				saida << categoria[i] << endl;
				tdscategorias.push_back(categoria[i]);
				saida.close();
			}
			else {
				cout << "Falha abertura arquivo saída Tdscategorias para inscrição de nova categoria" << endl;
				throw 1;
			}
			saida.open("doc/produtos/" + nome);
			if(saida.is_open()){
				saida << nome << endl << qtd << endl << preco << endl;
				saida.close();
			}
			else {
				cout << "Falha abertura arquivo saida novo produto" << endl;
				throw 1;
			}
		}
		if(verificaSeTemProd==0&&verificaSeTemCatCadas==4){
			saida.open("doc/produtos/" + nome);
			if(saida.is_open()){
				saida << nome << endl << qtd << endl << preco << endl;
				saida.close();
			}
			else {
				cout << "Falha abertura arquivo saida novo produto" << endl;
				throw 1;
			}
			saida.open("doc/categorias/" + categoria[i], ios::app);
			if(saida.is_open()){
				saida << nome << endl;
			}
			else {
				cout << "falha abertura arquivo saida para cadastrar o produto na categoria" << endl;
				throw 1;
			}
		}
	}
}
void PegaCategoriasRecomendadas(string cpf, vector<string> &categoriaAux){
	vector<string> categoria;
	vector<int> pesos;
	vector<int> pesosAux;
    vector<int> pesosAux2;
	int p, evitaRep, LinhaPesosRepetidos, PegaLinha, e;
	string c, aux, lixo;
	ifstream entrada;
	ofstream saida;

	categoria.clear();
	pesos.clear();
	//pego as categorias em que o cliente já comprou
	entrada.open("doc/clientes/" + cpf + "categorias", ios::in);
	if(entrada.is_open()){
		while(!entrada.eof()){
			getline(entrada,aux);
			cout << aux << endl;
			categoria.push_back(aux);
			entrada >> p;
			cout << p << endl;
			pesos.push_back(p);
        	pesosAux.push_back(p);
			getline(entrada,lixo);
		}
	}
	else{
		cout << "Falha abertura arquivo categorias recomendadas cliente" << endl;
		throw 1;
	}
	//ordenei vector pesos de maior para menor
    sort(pesos.begin(), pesos.end(), maior_menor);
    //recuperei a linha em que estava o peso antes de ser ordenado e salvei em pesosAux2
    for(int i=0; i<(pesos.size()-1); i++){
        evitaRep=0;
        for(int k=0; k<(pesosAux.size()-1); k++){
            if(pesos[i]==pesosAux[k]&&evitaRep==0&&e!=k){
                pesosAux2.push_back(k);
                evitaRep = 1;
                e=k;
            }
            else if(evitaRep==0){
                e=0;
            }
        }
    }
	//ordenei categoriaAux de acordo com o peso, de maior para menor
    for(int i=0; i<pesosAux2.size(); i++){
        PegaLinha=pesosAux2[i];
        c = categoria[PegaLinha];
        categoriaAux.push_back(c);
    }
    //Verifico a partir de qual linha os pesos se repetem
    for(int i=0; i<(pesos.size()-1); i++){
        if(pesos[i]<=pesos[i+1]){
            LinhaPesosRepetidos = i;
            i=1000000;
        }
    }
    //Reutilizo vector categoria com ele zerado e salvo as categorias com pesos iguais nele
    categoria.clear();
    for(int i=LinhaPesosRepetidos; i<categoriaAux.size(); i++){
        categoria.push_back(categoriaAux[i]);
    }
	//Ordeno categorias em ordem lexicográfica
	sort(categoria.begin(), categoria.end());
	//Ordeno o vector categoriaAux de acordo com peso e lexicografia
	for(int i=LinhaPesosRepetidos; i<categoriaAux.size(); i++){
        categoriaAux[i] = categoria[i-LinhaPesosRepetidos];
        
    }
	//Reescrevo no arquivo categorias recomendadas do cliente as categorias ordenadas
	saida.open("doc/clientes/" + cpf + "categorias");
	if(saida.is_open()){
		for(int i=0; i<categoriaAux.size(); i++){
			saida << categoriaAux[i] << endl << pesos[i] << endl;
		}
	}
	else{
		cout << "Falha abertura arquivo saida categorias recomendadas cliente" << endl;
		throw 1;
	}
}
void imprimeProdutosRecomendados(vector<string> &categoriaAux){
	ifstream entrada;
	ofstream saida;
	string aux;
	vector<string> produtos;
	int contador=0;

	produtos.clear();
	//Abro arquivo das categorias recomendadas e salvo os produtos em um vector produtos
	for(int i=0; i<categoriaAux.size(); i++){
		entrada.open("doc/categorias/" + categoriaAux[i]);
		if(entrada.is_open()){
			while(entrada&&contador<=20){
				getline(entrada,aux);
				produtos.push_back(aux);
				contador++;
			}
			entrada.close();
		}
		else{
			cout << "Falha abertura arquivo categorias" << endl;
			throw 1;
		}
	}
	//Imprimo produtos recomendados, no máximo 10
	cout << endl << "Produtos recomendados: " << endl;
	contador=1;
	for(int i=0; i<produtos.size(); i++){
		if(i%2==0||i==0){
			cout << contador << "°: " << produtos[i] << endl;
			contador++;
		}
	}
}
void imprimeProdutos(vector<string> &tdscategorias){
	ifstream entrada;
	ofstream sada;
	string prod;
	string qtd;
	string preco;
	vector<string> produtos;

	if(tdscategorias.size()==2){
		throw 2;
	}
	for(int i=1; i<(tdscategorias.size()-1); i++){
		entrada.open("doc/categorias/" + tdscategorias[i], ios::in);
		produtos.clear();
		if(entrada.is_open()){
			cout << endl << tdscategorias[i] << ":" << endl << endl;
			while(entrada){
				getline(entrada,prod);
				produtos.push_back(prod);
			}
			entrada.close();
		}
		else{
			cout << "falha abertura entrada categoria" << endl;
			throw 1;
		}
		for(int k=0; k<(produtos.size()-1); k++){
			entrada.open("doc/produtos/" + produtos[k]);
			if(entrada.is_open()){
				getline(entrada,prod);
				entrada >> qtd;
				entrada >> preco;
				cout << "Produto: "<< prod << endl << "Quantidade em estoque: " << qtd << endl << "Preço: R$"<< preco << ",00" << endl;
				cout << "--------------------------------------------------------" << endl;
				entrada.close();
			}
			else {
				cout << "falha abertura entrada imprimir produto" << endl;
				throw 1;
			}
		}
	}
}
int alteraClienteVeSeEhSocio(vector<string> &categoria, string cpf){
	ifstream entrada;
	ofstream saida;
	int socio, retornaSouNsocio=0;
	string aux;
	string lixo;
	int controlePeso;
	int contaPeso;
	int controleCat;
	vector<int> pesos;

	pesos.clear();
	entrada.open("doc/clientes/" + cpf);
	if(entrada.is_open()){
			entrada >> socio;
			if(socio>=3){
				socio++;
				retornaSouNsocio = socio;
			}
			else if(socio<3){
				socio++;
				retornaSouNsocio = socio;
			}
			entrada.close();
	}
	else{
		cout << "Erro abertura arquivo entrada cliente " << endl;
		throw 1;
	}
	entrada.open("doc/clientes/" + cpf + "categorias", ios::in);
	//cout << "aehhh" << endl;
	if(entrada.is_open()){
		for(int i=0; i<categoria.size(); i++){
			//cout << "tamanho vector categorias: " << categoria.size() << categoria[i] << endl;
 			controlePeso=0;
			while(!entrada.eof()&&controlePeso==0){	
				getline(entrada,aux);
				if(aux==categoria[i]){
					entrada >> contaPeso;
					contaPeso++;
					pesos.push_back(contaPeso);
					getline(entrada,lixo);
 					controlePeso=1;
				}
			}
			if(controlePeso==0){
				contaPeso=1;
				pesos.push_back(contaPeso);
			}	
		}
		entrada.close();
	}
	else{
		cout << "Falha abertura entrada categorias recomendadas" << endl;
		throw 1;
	}
	entrada.open("doc/clientes/" + cpf + "categorias", ios::in);
	if(entrada.is_open()){
		while(!entrada.eof()){
			getline(entrada,aux);
			//cout << "cat: " << aux << endl;
			controleCat=0;
			for(int i=0; i<categoria.size(); i++){
				//cout << "tamanho vector categorias: " << categoria.size() << categoria[i] << endl;
				if(aux==categoria[i]){
					controleCat = 1;
					entrada >> contaPeso;
					getline(entrada,lixo);
				}
			}
			if(controleCat==0){
				categoria.push_back(aux);
				entrada >> contaPeso;
				pesos.push_back(contaPeso);
				getline(entrada,lixo);
			}
		}
		entrada.close();
	}
	else{
		cout << "Falha abertura entrada categorias recomendadas" << endl;
		throw 1;
	}
	saida.open("doc/clientes/" + cpf + "categorias", ios::trunc);
	if(saida.is_open()){
		for(int i=0; i<categoria.size(); i++){
			//cout << "tamanho vector categorias: " << categoria.size() << categoria[i] << endl;
			saida << categoria[i] << endl << pesos[i] << endl;
			//cout << "linha: " << i << endl;
		}
		saida.close();
	}
	else{
		cout << "Falha abertura arquivo saída categorias recomendadas" << endl;
		throw 1;
	}
	return retornaSouNsocio;
}
void imprime_precoTotal(int s, float pT){
	if(s<3){
		cout << "Preço Total: R$" << pT << endl << "Desconto: 0" << endl << "Valor final: R$" << pT << endl;
	}
	else if(s>3){
		pT = pT * 0.90;
		cout << "Preço Total: R$" << pT << endl << "Desconto: 10%" << endl << "Valor final: R$" << pT << endl;
	}
}

int main(){
	int com=1, com1=-1, com2=-1, com3=-1;
	ofstream saida;
	ifstream entrada;
	string nome, tel, email, cpf, cpfCompara, cat, lixo;
	vector<string> tdscategorias;
	vector<Cliente *> cliente;
	vector<Produto *> product;
	vector<string> categoria;
	vector<string> categoriaAux;
	vector<int> peso;
	int carri=-1, qtd, aeh, qtda, SocioOuNao, ClienteJaCadastrado, socio=0;
	float precoTotal=0;
	string prod, aux;
	string parar = "Parar";
	float preco;
	try{
	while(com!=0){
		cout << "Bem-vindo ao mercado Vitória!" << endl << "Digite 1 para entrar no Modo venda" << endl;
		cout << "Digite 2 para entrar no Modo recomendação" << endl;
		cout << "Digite 3 para entrar no Modo estoque" << endl;
		cout << "Digite 0 para sair" << endl;
		com = getInput<int>();
		switch(com){
				case 1:
					tdscategorias.clear();
					categoria.clear();
					//Recupero as categorias presentes no estoque do mercado
					entrada.open("doc/categorias/Tdscategorias.txt");
					if(entrada.is_open()){
						while(!entrada.eof()){
							getline(entrada,cat);
							tdscategorias.push_back(cat);
							}
							entrada.close();
						}
					else { 
						cout << "Erro na abertura entrada para ler tdscategorias" << endl;
						throw 1;
					}
					while(com1!=0){
						cout << endl << "Digite 1 para inserir os dados do cliente" << endl << "Digite 0 para sair" << endl;
						com1 = getInput<int>();
						switch(com1){
							case 1:
								cout << "Nome:" << endl;
								nome = getString();
								cout << "Telefone:" << endl;
								tel = getString();
								cout << "Email:" << endl;
								email = getString();
								cout << "CPF:" << endl;
								cpf = getString();
								cout << "Lista de produtos vendidos:" << endl << endl;
								imprimeProdutos(tdscategorias);
								while(carri!=0){
									cout << endl << "Digite 1 para comprar" << endl << "Digite 2 para imprimir o total" << endl << "Digite 0 para sair" << endl;
									carri = getInput<int>();
									switch(carri){
										case 1:
											cout << "Escreva a categoria do produto: " << endl;
											cat = getString();
											aeh = 0;
											while(aeh==0){
												for(int y=0; y<tdscategorias.size(); y++){
													if(cat==tdscategorias[y]){
														aeh=1;
													}
												}
												if(aeh==0){
													cout << "Categoria inexistente!" << endl << "Digite novamente" << endl;
													cat = getString();
												}
											}
											cout << "Nome do produto: " << endl;
											prod = getString();
											aeh = 0;
											while(aeh==0){
												entrada.open("doc/categorias/" + cat);
												if(entrada.is_open()){
													while(!entrada.eof()){
														getline(entrada,aux);
														if(prod==aux){
															aeh=1;
														}
													}
													entrada.close();
												}
												else{
													cout << "falha abertura entrada categoria" << endl;
													throw 1;
												}
												if(aeh==0){
													cout << "Produto inexistente!" << endl << "Digite novamente:" << endl;
													prod = getString();
												}
											}
											cout << "Quantidade:" << endl;
											qtd = getInput<int>();
											aeh = 0;
											while(aeh==0){
												entrada.open("doc/produtos/" + prod);
												if(entrada.is_open()){
													getline(entrada,aux);
													entrada >> qtda;
													entrada >> preco;
													if(qtd>qtda || qtd<=0){
														cout << "Quantidade indisponível!" << endl << "Digite novamente:" << endl;
														qtd = getInput<int>();
													}
													else{
														aeh=1;
														qtda = qtda - qtd;
													}
													entrada.close();
												}
												else{
													cout << "falha abertura entrada produto 2" << endl;
													throw 1;
												}	
											}
											//Atualizo a quantidade do produto no estoque
											saida.open("doc/produtos/" + prod);
											if(saida.is_open()){
												saida << prod << endl << qtda << endl << preco << endl;
												saida.close();
											}
											else{
												cout << "Falha abertura saida produto" << endl;
												throw 1;
											}
											//Crio um objeto com novo produto
											product.push_back(new Produto(cat, prod, preco, qtd));
											categoria.push_back(cat);
											preco = preco * qtd;
											precoTotal+=preco;
											break;
										case 2:
											//Imprimo produtos comprados
											for(Produto * c: product){
												c->imprime_dados();
												cout <<"----------------------------------------------------------" << endl;
											}
											//Vejo se o cliente já está cadastrado
											entrada.open("doc/clientes/tdsclientes.txt");
											ClienteJaCadastrado = 0;
											if(entrada.is_open()){
												while(!entrada.eof()&&ClienteJaCadastrado==0){
													getline(entrada,aux);
													if(aux==cpf){
														SocioOuNao = alteraClienteVeSeEhSocio(categoria, cpf);
														ClienteJaCadastrado = 1;
													}
												}
												entrada.close();
												if(ClienteJaCadastrado==0){
													saida.open("doc/clientes/tdsclientes.txt", ios::app);
													if(saida.is_open()){
														saida << cpf << endl;
														saida.close();
													}
													else{
														cout << "Falha inclusão de novo cliente em lista" << endl;
													}
												}
											}
											else{
												cout << "Falha abertura arquivo entrada tdsclientes" << endl;
												throw 1;
											}
											//Cadastro o cliente se não estiver cadastrado
											if(ClienteJaCadastrado==0){
												cliente.push_back(new Cliente(nome, tel, email, cpf, socio));
												saida.open("doc/clientes/" + cpf);
												if(saida.is_open()){
													saida << socio << endl;
													saida << nome << endl;
    												saida << cpf << endl;
    												saida << tel << endl;    
    												saida << email << endl;
													saida.close();
												} 
											
												else{
													cout << "Falha criação cadastro novo cliente" <<endl;
													throw 1;
												}
												SocioOuNao=0;
												saida.open("doc/clientes/" + cpf + "categorias");
												if(saida.is_open()){
													for(int i=0; i<categoria.size(); i++){
														saida << categoria[i] << endl << "1" << endl;
													}
													saida.close();
												}
												else{
													cout << "Erro" << endl;
													throw 1;
												}

											}
											//altera a variável sócio, para acrescentar que houve mais uma compra
											if(ClienteJaCadastrado == 1){
												saida.open("doc/clientes/" + cpf);
												if(saida.is_open()){
													saida << SocioOuNao << endl;
													saida << nome << endl;
    												saida << cpf << endl;
    												saida << tel << endl;    
    												saida << email << endl;
													saida.close();
												}
												else{
													cout << "Erro alteração socio Cliente" << endl;
													throw 1;
												}
											}
											imprime_precoTotal(SocioOuNao, precoTotal);
											carri = 0;
											cout << "Venda finalizada!" << endl;
											carri = 0; com1 = 0; com=0;
											break;
										case 0:
											carri = 0; com1 = 0; com=0;
											break;
										default:
											cout << "Opção inválida" << endl;
											break;
									}

								}

								break;
		    				case 0:
								break;
					
							default:
								cout << "Opção inválida" << endl;
								break;
			
						}
					}
					break;

				case 2:
					//while para pedir as informações do cliente enquanto não informar um cliente cadastrado
					while(com2!=0){
						categoria.clear();
						peso.clear();
						cout << "Digite os dados do cliente:" << endl;
						cout << "Nome:" << endl;
						nome = getString();
						cout << "Telefone:" << endl;
						tel = getString();
						cout << "Email:" << endl;
						email = getString();
						cout << "CPF:" << endl;
						cpf = getString();
						entrada.open("doc/clientes/tdsclientes.txt");
						if(entrada.is_open()){
							ClienteJaCadastrado=0;
							while(entrada&&ClienteJaCadastrado==0){
								getline(entrada,cpfCompara);
								if(cpf==cpfCompara){
									ClienteJaCadastrado=1;
									com2=0;
								}
							}
							if(ClienteJaCadastrado==0){
								com2=-1;
								cout << "Cliente não cadastrado!" << endl;
							}
							entrada.close();
						}
						else{
							cout << "falha abertura arquivo entrada tdsclientes" << endl;
							throw 1;
						}
					}
					PegaCategoriasRecomendadas(cpf, categoriaAux);
					imprimeProdutosRecomendados(categoriaAux);
					com=0;
					break;

				case 3:
					int qtd;
					//vector<Produto *> product;
					//vector<string> categoria;
					//string nome;
					//string cat;
					float preco;
					while(com3!=0){
						cout << endl << "Digite 1 para cadastrar um novo produto" << endl;
						cout << "Digite 0 para sair do modo estoque" << endl;
						com3 = getInput<int>();	
						switch(com3){
							case 1:
								tdscategorias.clear();
								categoria.clear();
								cout << "Digite todas as categorias do produto, quando acabar digite 'Parar':" << endl;
								cat = getString();
								categoria.push_back(cat);
								while(cat!=parar){
									cat = getString();
					 				if(cat!=parar){
					  					categoria.push_back(cat);
									}
								}
								cout << "Nome do produto:" << endl;
								nome = getString();
								cout << "Preço:" << endl;
								preco = getInput<float>();
								cout << "Quantidade:" << endl;
								qtd = getInput<int>();
					            product.push_back(new Produto(categoria, nome, preco, qtd));
								entrada.open("doc/categorias/Tdscategorias.txt");
								if(entrada.is_open()){
									while(!entrada.eof()){
										getline(entrada,cat);
										tdscategorias.push_back(cat);
									}
									entrada.close();
								}
								else { 
									cout << "Erro na abertura entrada para ler tdscategorias" << endl;
									throw 1;
								}
								criaArquivoEstoque(categoria, tdscategorias, nome, preco, qtd);
								break;
							case 0:
								com=0;
								break;
							default:
								cout << "Opção inválida!" << endl;
								break;
						}
					}
					break;
				
				case 0:
					break;
			
				default:
					cout << "Opção inválida" << endl;
					break;
		}
		
	}
	}
	catch(int erro){
		if(erro==1){
			cout << "Falha abertura arquivo" << endl;
		}
		else if(erro==2){
			cout << "Estoque vazio" << endl;
		}
	}
	


	return 0;
}




