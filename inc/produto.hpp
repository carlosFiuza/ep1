#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <vector>

using namespace std;

class Produto{
	private:
		vector<string> categoria;
		string categori;
		string nome;
		float preco;
		int qtd;
		Produto();

	public:
		Produto(vector<string> &categoria, string nome, float preco, int qtd);
		Produto(string categori, string nome, float preco, int qtd);
		~Produto();

	string get_categoria();
	void set_categoria(vector<string> &categoria);
	string get_categori();
	void set_categori(string categori);
	string get_nome();
	void set_nome(string nome);
	float get_preco();
	void set_preco(float preco);
	int get_qtd();
	void set_qtd(int qtd);
	void imprime_dados();
};

#endif
