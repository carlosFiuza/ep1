#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente{
	private:
		string nome;
		string telefone;
		string email;
		string cpf;
		int socio;
		Cliente();
	public:
		Cliente(string nome, string telefone, string email, string cpf, int socio);
		~Cliente();

		string get_nome();
		void set_nome(string nome);
		string get_telefone();
		void set_telefone(string telefone);
		string get_email();
		void set_email(string email);
		string get_cpf();
		void set_cpf(string cpf);
		int get_socio();
		void set_socio(int socio);
};

#endif
